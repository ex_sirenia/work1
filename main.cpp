/**************************/
/*     Egor Plotnikov     */
/*         KE-317         */
/**************************/
#include <iostream>

using namespace std;

int main() {
	int a, b;
	
	cout << "A&b";
	cin >> a >> b;
	cout
		<< "A - B" << (a - b) << endl
		<< "A + B" << (a + b) << endl
		<< "A * B" << (a * b) << endl;
	
	return 0;
}